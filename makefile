default_target: launch

install:
	@npm install

launch: install
	@npm run serve

build: install
	@npm run build
